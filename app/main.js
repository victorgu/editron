const fs = require('fs');
const { app, BrowserWindow, dialog, Menu } = require('electron');

let mainWindow = null;

app.on('ready', () => {
  // console.log('The application is ready.');
  mainWindow = new BrowserWindow({
    show: false,
    webPreferences: {
    nodeIntegration: true
    }
  });

  Menu.setApplicationMenu(applicationMenu);

  mainWindow.loadFile(`${__dirname}/index.html`);

  // getFileFromUser();

  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
});

exports.getFileFromUser = () => {
  let file;
  dialog.showOpenDialog(mainWindow, {
    properties: ['openFile'],
    buttonLabel: 'Abrir ahora',
    title: 'Abrir Editron document',
    filters: [
      { name: 'Text files', extensions: [ 'txt', 'text'] },
      { name: 'Markdown files', extensions: [ 'md', 'mdown', 'markdown'] }
    ]
  }).then(result => {
    // console.log(result.canceled);
    // console.log(result.filePaths);
    if(!result.filePaths) return;

    file = result.filePaths[0];
    openFile(file);

  }).catch(err => {
    console.log(err);
  });
};

exports.saveMarkdown = (file, content) => {
  
  if (!file) {
    file = dialog.showSaveDialogSync(mainWindow,{
      title: 'Save Markdown',
      defaultPath: app.getPath('desktop'),
      filters: [
        { name: 'Text files', extensions: [ 'txt', 'text'] },
        { name: 'Markdown files', extensions: [ 'md', 'mdown', 'markdown'] }
      ]
    });
  };

  if (!file) return;

  fs.writeFileSync(file, content);
  openFile(file);
};

exports.saveHtml = content => {
  const file = dialog.showSaveDialogSync(mainWindow, {
    title: 'Save HTML',
    defaultPath: app.getPath('desktop'),
    filters: [{ name: 'HTML files', extensions: [ 'html'] }]
  });

  if (!file) return;

  fs.writeFileSync(file, content);

};

const openFile = (exports.openFile = file => {
  const content = fs.readFileSync(file).toString();
  app.addRecentDocument(file);
  mainWindow.webContents.send('file-opened', file, content);
});

const template = [
  {
    label: 'file',
    submenu: [
      {
        label: 'Open file',
        accelerator: 'CommandOrControl+O',
        click() {
          exports.getFileFromUser();
        }
      },
      {
        label: 'Save file',
        accelerator: 'CommandOrControl+S',
        click() {
          mainWindow.webContents.send('save-markdown');
        },
      },
      {
        label: 'Save HTML',
        accelerator: 'CommandOrControl+Shift+S',
        click() {
          mainWindow.webContents.send('save-html');
        },
      },
      {
        label: 'Copy',
        role: 'copy',
      },
    ]
  }
];

if (process.platform === 'darwin') {
  const applicationName = 'Editron';
  template.unshift({
    label: applicationName,
    submenu: [
      {
        label: `About ${applicationName}`,
        role: 'about',
      },
      {
        label: `Quit ${applicationName}`,
        role: 'quit',
      },
    ]
  });
} 

const applicationMenu = Menu.buildFromTemplate(template);